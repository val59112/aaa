---
title: "Structure de données : 8_ Arbres binaires"
subtitle: "TP_1 : arbre binaire "
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : Dessinons... 🏆 

Dessiner tous les arbres binaires ayant respectivement 3 et 4 noeuds.

# Exercice 2 : Affichons... 🏆  

1. Ecrire une fonction affiche(ab) qui imprime un arbre un arbre sous la forme suivante :  

* pour un arbre vide, on n'imprime rien ;
* pour un noeud, on imprime une parenthèse ouvrante;
* son sous-arbre gauche (récursivemment) sa valeur, son sous-arbre droit (récursivement) ; 
* une parenthèse fermante ;  
Ainsi pour l'arbre :  

--------pas d'img--------  

```python
>>> affiche(ab)
((B(C))A(D))
```

2. Dessiner l'arbre dont la sortie est : (1((2)3)).  

#  Exercice 3 : Complétons... 🏆  

Ajoutons à la classe Noeud une méthode __eq__ permettant de tester l'égalité entre deux arbres binaires à l'aide de l'opération ==.

# Exercice 4 : Renvoyons des arbres binaires particuliers  🏆  

1. Ecrire une fonction `parfait(h)` qui reçoit en argument un entier `h` supérieur ou égal à zéro et renvoie un arbre binaire parfait de hauteur h dont les noeuds sont "o" par défaut.  

2. Ecrire une fonction `peigne_gauche(ab)` qui reçoit en argument un entier `h` supérieur ou égal à zéro et renvoie un peigne de hauteur h où chaque noeud a un sous-arbre droit qui est vide.  

3. Ecrire une fonction `est_peigne_gauche(ab)` qui renvoie `True` si et seulement _ab_ est un peigne gauche

# Exercice 5 : Parcours en profondeur🏆🏆  

Soit l'arbre binaire suivant :  
--------pas d'img--------  

1. Implémenter cet arbre binaire avec la classe Noeud.  

2. Donner les parcours en profondeur :  

* préfixe  

```

```

* infixe

```

```

* postfixe  

```

```

On vérifiera avec les algorithmes du cours l'ordre obtenu.

# Exercice 6 : Parcours en Largeur  
  
Parcours_largeur(arbre)  
Creer une file F vide  
Creer une liste P vide  
Enfiler arbre  
Ajouter à P la valeur de l'arbre  
TANT QUE F n'est pas vide  
|  elemen=tete de F  
|  SI element n'est pas vide  
|  |  enfiler sous arbre gauche  
|  |  ajouter valeur à P  
|  |  enfiler sous arbre droit  
|  |  ajouter valeur à P  
|  |  defiler F  
|  renvoyer P
  
Sources :  
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen    
* NSI Terminale Nathan les vrais exos S Pasquet M Leopoldoff 
