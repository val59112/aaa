### ECE 2 ###
def moyenne(tab):
    """Renvoi la moyenne d'une liste d'entiers
@entree
    : tab : list
@sortie
    : float :
    """
    if len(tab)==0:
        return 'erreur'
    else:
        return sum(tab)/len(tab)
print('ECE2/Exercice 1\n\n\n')
print('>>> moyenne([5,3,8])')
print(moyenne([5,3,8]))
print('>>> moyenne([])')
print(moyenne([1,2,3,4,5,6,7,8,9,10]))
print('>>> moyenne([])')
print(moyenne([]))

# Exercice 2

def tri(tab):
    """tri un tableau contenant des 0 et des 1  et les tries sous la forme [ zone des 0 , zone des 1 ]
@entree
    : tab : list
@sortie
    : list :
    """
    #i est le premier indice de la zone non triee, j le dernier indice.
    #Au debut, la zone non triee est le tableau entier.
    i=0
    j=len(tab)-1
    while i!= j :
        if tab[i]== 0:
            i=i+1
        else :
            valeur= tab[j]
            tab[j] = tab[i]
            tab[i]= valeur
            j=j-1
    return tab
print('tri([0,1,0,1,0,1,0,1,0])')
print(tri([0,1,0,1,0,1,0,1,0]))