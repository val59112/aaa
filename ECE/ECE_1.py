### ECE 1 ###
from math import sqrt   # import de la fonction racine carrée pour ex 2

# Exercice 1
def rechercher(tab,n):
    """Renvoie la derniere occurence d'un element n donne dans une liste
@entree
    : tab : list
    : n : int
@sortie
    : int :
"""
    acc=0
    index=-1
    for i in tab:
        if i==n:
            index=acc
        acc+=1
    if index==-1:
        return len(tab)
    return index
print('ECE1/Exercice 1\n\n\n')
print('>>> rechercher([5,3],1)')
print(rechercher([5,3],1))
print('>>> rechercher([2,4],2)')
print(rechercher([2,4],2))
print('>>> rechercher([2,3,5,2,4],2)')
print(rechercher([2,3,5,2,4],2))

# Exercice 2

def distance(point1,point2):
    """Calcule et renvoie la distance entre deux points.
@entree
    : point1,point2 : tuple
@sortie
    : float :
    """
    assert type(point1)==tuple,"point1 not tuple"
    assert type(point2)==tuple,"point2 not tuple"
    return sqrt((point1[0]-point2[0])**2+ (point1[1]-point2[1])**2)

assert distance((1, 0), (5, 3)) == 5.0, "erreur de calcul"

def plus_courte_distance(tab, depart):
    """ Renvoie le point du tableau tab se trouvant à la plus courte distance du point depart.
@entree
    : tab : list of tuple
    : depart : tuple
@sortie
    : tuple :
    """
    assert type(tab)==list,"tab is not a list"
    for element_de_tab in tab:
        assert type(element_de_tab)==tuple
    point = tab[0]
    min_dist= distance(point,depart)
    for i in range (1,len(tab)-1):
        if distance(tab[i], depart)<min_dist:
            point = [tab[i][0],tab[i][1]]
            min_dist= distance(tab[i], depart)
    return point

assert plus_courte_distance([(7, 9), (2, 5), (5, 2)], (0, 0)) == [2, 5], "erreur"
print('\n\n\n ECE1/Exercice 2\n\n\n')
print('>>> distance((1, 0), (5, 3))')
print(distance((1, 0), (5, 3)))
print('>>> plus_courte_distance([(7, 9), (2, 5), (5, 2)], (0, 0))')
print(plus_courte_distance([(7, 9), (2, 5), (5, 2)], (0, 0)))