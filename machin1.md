---
title: "Architecture de la machine : 7 Processus et ressources"
subtitle: "cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

![BO](./img/BO.jpg)

Lorsqu'on utilise un ordinateur, nous "ouvrons" souvent plusieurs applications en même temps : un navigateur Web avec plusieurs pages, un traitement de texte, un diaporama, un logiciel d'écoute de musiques.... Tous ces programmes s'éxécutent en même temps. Pourtant, un ordinateur ne dispose que d'un nombre limité de processeurs. Or un programme n'est qu'une suite d'instructions en langage machine exécutées une à une par le processeur.  
Les instructions ne __se font donc pas "en même temps"__  
Cette exécution concurrente de programmes est l'une des fonctionnalités de base offerte par les systèmes d'exploitation : ce sont des systèmes d'exploitation multitâches. 

# Processus et ressources partagées

>📢 Définition : Un __exécutable__ est un fichier binaire contenant des instructions machines directement exécutables par le processus de la machine.  

Concrètement, quand on exécute un programme en cliquant sur l'icône depuis l'interface graphique (GUI : Graphical User Interface) ou depuis un terminal (CLI : Command Line Interface), l'OS effectue les actions suivantes:  

* le fichier contenant le programme est copié dans la mémoire RAM, à une certaine adresse _a_ ;
* le système d'exploitation écrit la valeur _a_ dans le registre __IP__ ( Instruction Pointer).

Au prochain cycle d'horloge du processeur, ce dernier va lire l'instruction se trouvant à l'adresse _a_ et l'éxécuter. Ensuite il exécutera la seconde instruction et ainsi de suite...

Cependant, d'autres processus souhaitent être exécutés peut être...il existe donc un __gestionnaire d'interruption__ : une interruption est un signal envoyé au processeur lorsqu'un événement se produit ( disque dur qui signale qu'il a fini d'écrire par exemple). Ce programme  reçoit en argument une copie des valeurs courante des registres, ainsi qu'un code numérique lui permettant de connaître le type d'interruption.  
Ainsi l'horloge du processeur fait lui même des interruptions à intervalle de temps fixe ce qui permet d'exécuter des programmes de façon concurrente.  

![interruption](./img/interruption.jpg)  

>📢 Définition : on appelle __processus__ une __instance d'un programme__ chargé en mémoire qui sera exécutée de manière continue ou discontinue par le système d'exploitation.  

![processus](./img/processus.jpg)

Celui-ci identifie généralement les processus par un numéro unique (PID).  Un processus est décrit par : 

* un ensemble d'instructions à exécuter (section code)
* un espace d'adressage en mémoire vive pour travailler (sections pile, tas et data)
* des ressources (fichiers ouverts, socket réseaux, connexion bdd…)
* un environnement (PID, répertoire, utilisateur, droits, priorité, temps, processus parent, variables d'environnement, états des registres CPU...)
* des flux d'entrée (stdin) et de sortie (stdout, stderr) pour communiquer avec l’extérieur

# Ordonnanceur du système d'exploitation

## Gestionnaire d'interruption
Le système d'exploitation peut configurer une horloge et le __gestionnaire d'interruption__ prendra la main à intervalles réguliers.  

 Voyons sur un exemple le fonctionnement.  

![ordonnanceur](./img/ordonnanceur.jpg)

Le fait que l'ordonnanceur interrompt un processus et sauve son état s'appelle __commutation de contexte.__
Afin de choisir parmi tous les processus, le système d'exploitation conserve pour chaque processus une structure de données nommée __PCB__ ( Process Control Bloc). C'est une zone mémoire dans laquelle sont stockées diverses informations sur le processus.  

|Nom|Description|
|:---|:---|
|PID| _Process ID_, identifiant numérique du processus|
|Etat| état dans lequel se trouve le processus|
|Registres| la valeur des registres lors de sa dernière interruption|
|Mémoire| zone mémoire ( plage d'adresses) allouée au processus lors de son exécution|
|Ressources| liste des fichiers ouverts, connexions réseaux en cours d'utilisation, ...|

L'ordonnanceur conserve les PCB dans une structure de données ( par exemple une file). Le premier processus dans la file reprend son éxécution. Lors de la prochaine interruption, il est mis en bout de file, etc...  

![](./img/com_con.jpg)  ![](./img/ordP.jpg)

## Fonctionnement de l'ordonnanceur

L'ordre d'exécution des processus et leur temps d'exécution alloué est décidé par l'algorithme. Plusieurs méthodes existent comme :
|Algorithme|Description|
|:---|:---|
|__Round-Robin__ (ou méthode du tourniquet)| chaque processus aura le processus pendant un certain quantum de temps | 
|__FIFO__ (First In First Out)|Premier processus exécuté, premier à utiliser le CPU| 
|__SJF__ (Shortest Job First)|Le processus le plus court en premier| 
|__RMS__ (Rate Monotonic S.)|Le processus qui a la plus petite période à la plus grande priorité| 
|A __priorité fixe__|Le processus le plus prioritaire a le processeur| 

Il n'existe pour le moment pas d'algorithme idéal. Ils peuvent être de deux types :

* en temps partagé ou coopératif : partage le temps d'exécution entre les processus en se basant sur des règles de priorité.
* en temps réel ou préemptif : optimise le temps d'exécution des processus afin que les processus devant se terminer à un instant fixé le soit effectivement. Ces algorithmes sont aussi capables de mettre en pause les processus afin d'exécuter un processus particulier lorsqu'un évènement extérieur survient.  

![](./img/P1P2P3.jpg)  

 Une fois leur ordre et leur temps d'exécution déterminé, les processus sont enfilés dans la file d'attente de l'ordonnanceur. Les processus sont exécutés les uns après les autres par le processeur.

# Gestion de processus

## Etats des processus

Tous les processus qui cohabitent en mémoire à un instant donné ne sont pas forcément en attente du processeur. Il se peut qu'un processus attende une ressource, un résultat ou un autre processus : par exemple si un logiciel de lecture vidéo est utilisé en streaming, les données arrivant via le réseau, il peut être en attente de nouvelles données. L'ordonnanceur n'allouera pas un quantum  de temps processeur car le processus ne peut avancer.  

La plupart des systèmes d'exploitation utilisent les états suivants : 

* __nouveau__ : processus en cours de création ; le système d'exploitation vient de copier l'éxécutable en mémoire et d'initier le PCB  
* __prêt__ : le processus peut être le prochain à s'éxécuter. Il est dans la "file d'attente" des processus.  
* __en exécution__ : le processus est en train de se terminer  
* __en attente ou bloqué__ : le processus est interrompu et en attente d'un événement externe : entrée, sortie, allocation mémoire,....
* __endormi__ : le processus dort  
* __terminé__ : le processus s'est terminé, le système d'exploitation est en train de désallouer les ressources que le processus utilisait.  
* __zombie__ : cas particulier où un processus fils se termine alors que son père est déjà terminé. Devenu orphelin, le système le place en état de zombie pour pouvoir récupérer ses ressources (mémoire, fichiers...)  

![](./img/cycle_de_vie.jpg)  

Un processus peut se terminer de façon anormale quel que soit l'état dans lequel se trouve le processus. En effet cela peut être une erreur provoquée par le programme( division par 0!), une erreur matérielle ( disque dur défectueux) ou enfin si l'utilisateur interrompt manuellement.

## Commandes Unix de gestion de processus  

La commande `ps` (process status) permet d'obtenir des informations sur les processus _statiques_ en cours d'exécution. `
```
$ ps aux
``` 
Les options -a -u -x permettent d'afficher tous les processus, d'afficher le nom des utilisateurs et de compter aussi les processus non lancé depuis un terminal.  
Chaque processus possède un identifiant nommé PID, Process Identification, sous la forme d’un nombre.
Le premier processus créé au démarrage du système d’exploitation aura pour PID la valeur 0, le deuxième
1, etc....  _remarques_ :

* %CPU : taux d'occupation du processeur
* %MEM : taux d'occupation de la mémoire
* TTY : identifiant du terminal où le processus a été lancé. ( "?" non lancé depuis un terminal)

![](./img/STAT.jpg)  

* STAT : indique l'état du processus.  
d'après https://askubuntu.com/questions/1144036/process-status-of-s-s-s-sl :  

![](./img/pid.jpg)



Le parent d’un processus sera identifié par son PPID, Parent Process Identification, permettant ainsi de tracer un processus.

![](./img/PPID.jpg)

La commande `top` affiche en temps réel des informations similaires à celles affichées par `ps` toute les secondes :  

![](./img/algo_ordo.jpg)

On observe ainsi quel processus occupe le plus le processeur. On parle alors de liste de processus _dynamique_.  

Pour interrompre un processus dont on connait le PID, on peut utiliser la commande `kill` :

```
$ kill 11164 1364
```

La commande va envoyer un signal de terminaison aux deux processus listés. Ce signal peut être intercepté par l'application qui peut par exemple vous proposer d'enregistrer avant de fermer. Si on souhaite terminer immédiatement on ajoute l'option -9 :  

```
$ kill -9 11164 1364
```

# Thread ou tâche

>📢 définition : Un thread est l'exécution d'une suite d'instructions démarrée par un processus ou encore appelé en français _processus léger_.  

Deux threads sont __l'exécution concurrente__ de deux suites d'instructions d'un même processus. Par exemple , pour un navigateur web, il peut y avoir un _thread_ dont le rôle est de dessiner la page web dans une fenêtre eu un autre _thread_ dont le rôle est de télécharger un fichier sur lequel l'utilisateur a cliqué. __La différence fondamentale__ entre processus et thread est que les processus ne partagent pas leur mémoire, alors que les _threads_, issus d'un même processus, peuvent accéder aux variables globales du programme et utilisent le même espace mémoire.  

![thread](./img/thread.jpg)

# Interblocage
Soit un système possède une instance unique de chacun des deux types de ressources R1 et R2. Un processus P1 détient l’instance de la ressource R1 et un autre processus P2 détient l’instance de la ressource R2. Pour suivre son exécution, P1 a besoin de l’instance de la ressource R2, et
inversement P2 a besoin de l’instance de la ressource R1. Une telle situation est une situation __d’interblocage__. 

![](./img/interblocage.jpg)

>📢 définition : Un ensemble de processus est dans une situation d’interblocage si chaque processus de l’ensemble
attend un événement qui ne peut être produit que par un autre processus de l’ensemble.

Les interblocages sont évidemment indésirables. Dans un interblocage, les processus ne terminent jamais leur exécution et les ressources du système sont immobilisés, empêchant ainsi d’autres travaux de commencer.  
Une situation d’interblocage peut survenir si les quatre conditions suivantes se produisent simultanément (Habermann) :

1. Accès exclusif : Les ressources ne peuvent être exploitées que par un seul processus à la fois.
2. Attente et occupation : Les processus qui demandent de nouvelles ressources gardent celles qu'ils ont déjà acquises et attendent la satisfaction de leur demande
3. Pas de réquisition : Les ressources déjà allouées ne peuvent pas être réquisitionnées.
4. Attente circulaire : Les processus en attente des ressources déjà allouées forment une chaîne circulaire d'attente.

Sources :  
* Cours Université de Lille DIU P Marquet  
* NSI Terminale Ellipses T. Balabonski,S. Conchon, JC. Filliatre, K Nguyen  
* NSI Terminale Nathan les vrais exos S Pasquet M Leopoldoff  
* cours LMD Loukam Mourad  
* cours Niort lycée Saint André A MAROT D SALLÉ J SIMONNEAU  
